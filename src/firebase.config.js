import { initializeApp } from "firebase/app";
import {getFirestore} from 'firebase/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyAeKIAMlihRuNtMjyyTrSU1CbG2bCuCMOs",
  authDomain: "house-marketplace-app-8cc92.firebaseapp.com",
  projectId: "house-marketplace-app-8cc92",
  storageBucket: "house-marketplace-app-8cc92.appspot.com",
  messagingSenderId: "829443979826",
  appId: "1:829443979826:web:9c50c18d45125e79d79a2b"
};

// Initialize Firebase
initializeApp(firebaseConfig);
export const db = getFirestore()